+++
title = 'About Me'
date = 2023-11-24T17:06:20-05:00
+++

Technical explorer, who can chart a course to achieve big goals and navigate through the storm, bringing the whole crew along in safety and fun.
Looking to bring over two decades of development and leadership experience to support the growth of an organization.

Experienced lead software engineer with 25 years in the technology industry dedicated to continuous learning, open source software, and Agile methodologies
A software engineer and servant leader who loves technology and learning. Supporter of agile methodologies and open-source software. Researcher and problem solver. Dependable, funny, caring, and calm under pressure.

Big picture thinker. Thrives on experimentation, collaboration, and scientific process.

:w
