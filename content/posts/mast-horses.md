+++
title = 'Mast Horses'
date = 2023-12-28T14:00:00-05:00
draft = false
+++

At our club there are many boaters, including myself, who take down our masts for winter storage. These masts from sailboats, over 30 feet in length, weigh several hundred pounds, and there are points where we have possibly a dozen or more masts in a staging area.  I've seen the sawhorses under these masks fail a couple of times. Masts are pretty heavy and when things are shifting or rolling, the weight of the mast is on a single point on a sawhorse. Even sawhorses which are rated for more weight than the mast have crumbled under this focused load.

I searched for a different type of sawhorse that would be both compact enough to store several and not take up too much space, and strong enough to handle the load and the roll of a large mast. I didn't find anything so, I've come up with a new design for mast horses using 2x4 lumber.

**Design requirements summary**:
1. Support large mast of a 30+ foot sailing vessels for working on mast preparation or take down
2. Be reasonably compact for storage most of the year
3. Avoid catastrophic failure under load and rolling of the mast

**Solution**:
A folding or scissoring horse made from common lumber. This design is fairly compact and supports the mast in a V shape extension of the legs. The key feature is that this horse holds the mast at four points, not just one, distributing the load onto the legs.

![Unfolded horse](https://github.com/davenuman/mast-horse/raw/main/assets/unfolded.jpg)

Download designs: <img alt="Design image" src="https://raw.githubusercontent.com/davenuman/mast-horse/main/assets/mast-horse.png" width="200" style="float:right" />
- [PDF A4 portrait](https://github.com/davenuman/mast-horse/raw/main/assets/mast-horse.pdf)
- [FreeCAD Design](https://github.com/davenuman/mast-horse/raw/main/assets/MastHorse.FCStd)

Design advantages:
 - Each mast horse supports the mast in 4 points, one for each leg, unlike a typical sawhorse which only has one point of contact to accept the load
 - Easy to build with scrap wood


## Operation

Stand the mast-horse on the ground and unfold it. The legs should rest on the top rails firmly, and the resulting angle of the top "V" should be about 30 degrees.

Folded up, it doesn't take up much space:
<img src="https://github.com/davenuman/mast-horse/raw/main/assets/folded.jpg" width="300" />

A pair of mast-horses should be enough to support many sizes of masts. Perhaps three sets are needed for some.
![Supporting the mast](https://github.com/davenuman/mast-horse/raw/main/assets/mast.jpg)

## Conclusion

Is it worth it? The new sawhorse on the left failed under the weight of a mast. Thankfully, no one was injured, and the only damage was to the sawhorse. I have seen similar with metal frame horses. The wooden mast horse may fail someday as well, and we can repair it and improve the design.

![Two saw horses compared](https://github.com/davenuman/mast-horse/raw/main/assets/compare.jpg)
