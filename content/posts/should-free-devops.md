---
title: Should-Free DevOps
subtitle: subtitle
date: 2023-12-01T17:07:57-05:00
keywords: devops should twelve-factor
---

If you are a DevOps Engineer, you probably have a list of _shoulds_. "I should learn those five hot new tools someone mentioned recently." "I should refactor that old code." "I should migrate all our tools to microservices in Kubernetes." "I should do a threat analysis, and review our monitoring and alert systems, and do something with AI somehow, and," ...and you get the idea.

It can be a little overwhelming.

I'm not writing this to tell you that you shouldn't do any of these things. Rather, I'd like to look at what the "should" thinking does to us. These "_should"_ statements have an impact on our wellbeing, which has been called "shoulding on yourself" by [psychologist Clayton Barbeau](https://www.palomar.edu/users/jtagg/should.htm).

We create expectations for ourselves with these "should" statements that are often unmet. These shoulds might come from others on our team, from social media, and from inside our own heads. The shoulding can create feelings of guilt, anxiety, stress, overwhelm, cognitive distortion

So how do we avoid this dangerous S word? We can change our "should" into "could" thinking. The Agile and DevOps mindset actually help us here with focusing on continuous improvement and smaller release cycles.

Let's look at an example: "We should have a full automated test coverage on our application." Sounds good. Who wouldn't want that? But if you are starting with no testing pipeline at all, this is daunting. Add to this a waterfall style deadline, and the likely outcome is stress plus inaction.

Instead, we could start with one simple test. If we are building a web service, the simplest test is to confirm a 200 status code is returned. So before all the discovery work of what testing tools to use and documenting best practices, we _could_ just use curl and start building our pipeline. Doesn't matter if you are using Jenkins, GitLab, GitHub, or something else, you can add a few lines of bash script into the process:

```
#!/usr/bin/env bash

TEST_URL="https://example.com/"

if [ "$(curl -s -o /dev/null -w %{http_code} ${TEST_URL})" -eq 200 ]; then
  echo PASS
else
  echo FAIL
fi
```

Changing the language from _should_ to _could_ takes us out of a place of mental resistance to a place of action. Working with the simple curl test, we can build our pipeline and once we have that working we're in a place where we can easily start thinking of what else we _could_ add to it. Instead of adding expectations, we add value.

That's a nice example, but what if the _shoulds_ keep coming? Here is a strategy to deal with them: [Notice, Pause, Explore, Clarify, Act](https://therapyinanutshell.com/shoulding/).

![Shoulds swear jar](/images/swearjar.jpg#floatright)

First, we need to **notice** when we are shoulding on ourselves. Jot it down. Make a list. Then we need to **pause**. It is human nature to jump to judgment, but acceptance is a much better starting place. We can accept the "should" wherever it came from, externally or internally, and accept how it makes us feel. After acceptance, we can **explore** our "shoulds" and evaluate them critically. Are they beneficial goals for me, even in part? Basically, ask, "why should I?"

This brings us to **clarify**. After we accept and evaluate, we are in a better frame of mind to change our "I should" into "I could" statements. Don't worry about the waterfall, delve into the DevOps strategies of small iterative actions. Now is a good time to define something you can choose to do or not do.

And finally, we **act**. Without the stress and anxiety of the "should" we have more potential focus and mental energy for action. When complete, these actions might inspire the next actions and valuable results. Or they might produce more "shoulds", but we have a strategy for those now.

Want some practice? Review the [Twelve-factor App](https://12factor.net/), and you might feel the "shoulds" piling on. It's good to notice them and then explore what might be valuable, clarify and define some next steps. On top of all our other work, maybe just try one action this next week.

---

**Disclaimer:** I do not have the breadth of knowledge to professionally evaluate these psychology claims nor the linked content in this article. I can say that this shift in thinking has helped me personally. I don't expect it to be life-changing, but I figure it _should_ be harmless to try.
